#include <iostream>
#include <iostream>

//#include <opencv2/opencv.hpp>
#include <opencv/cv.hpp>

using namespace cv;
using namespace std;


int main() {
    VideoCapture cap(0);
    namedWindow("Affichage");

    Mat image, edge;


    while(waitKey(1) < 0)
    {
        cap >> image;
        cvtColor(image, image, CV_BGR2GRAY);

        Canny(image, edge, 50, 100);
        imshow("Affichage", edge);
    }

    return 0;
}